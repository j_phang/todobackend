const jwt = require("jsonwebtoken");

exports.isAuthenticated = (req, res, next) => {
  try {
    var token = req.headers.authorization.split("Bearer ")[1];

    if (token) {
      jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
        if (err) {
          return res.status(403).send("Failed to authenticate token");
        } else {
          req.decoded = decoded;
          userId = decoded.id;
          next();
        }
      });
    } else {
      throw new Error();
    }
  } catch (err) {
    return res.status(403).send("No token provided");
  }
};
