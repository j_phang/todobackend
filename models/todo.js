const mongoose = require("mongoose");

const schema = mongoose.Schema;

const todoSchema = new schema(
  {
    title: {
      type: String,
      required: [true, "This field is mandatory"],
      unique: [true, 'Email is not avalaible'],
    },
    done:{
        type: Boolean,
        required: [true, "This field is mandatory"],
    },
    userId: {
        type: String,
        required: [true, "This field is mandatory"]
    }
  },
  {
    collection: "todo",
  }
);

const todo = mongoose.model("todo", todoSchema);

module.exports = todo;
