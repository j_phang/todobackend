const mongoose = require("mongoose");

const schema = mongoose.Schema;

const userSchema = new schema(
  {
    username: {
      type: String,
      required: [true, "This field is mandatory"],
      unique: [true, 'Email is not avalaible'],
      match: [/\S+@\S+\.\S+/, 'Email format is invalid']
    },
    password:{
        type: String,
        required: [true, "This field is mandatory"],
    },
    token: {
        type: String,
        required: false
    }
  },
  {
    collection: "user",
  }
);

const user = mongoose.model("user", userSchema);

module.exports = user;
