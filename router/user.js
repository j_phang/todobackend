var express = require("express");
const app = express.Router();

const { isAuthenticated } = require("../middleware/auth");
const { createUser, loginUser, updateUser } = require("../controller/user");

app.route('/').post(createUser).put(isAuthenticated, updateUser);
app.route('/login').post(loginUser)

module.exports = app;
