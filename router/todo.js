// var express = require("express");
// const app = express.Router();
// const { isAuthenticated } = require('../middleware/auth')

// const {
//   getTodo,
//   getTodoId,
//   createTodo,
//   putTodo,
//   deleteTodo,
// } = require("../controller/todo");

// app.get(isAuthenticated, getTodo).post(isAuthenticated,  createTodo).put(isAuthenticated,  putTodo);

// app.route('/:id').get(isAuthenticated,  getTodoId).delete(isAuthenticated, deleteTodo);

// module.exports = app;

var express = require("express");
const app = express.Router();

const {
  getTodo,
  createTodo,
  putTodo,
  getTodoId,
  deleteTodo,
} = require("../controller/todo");
const { isAuthenticated } = require("../middleware/auth");

app
  .route("/")
  .get(isAuthenticated, getTodo)
  .post(isAuthenticated, createTodo)
app
  .route("/:id")
  .get(isAuthenticated, getTodoId)
  .put(isAuthenticated, putTodo)
  .delete(isAuthenticated, deleteTodo);

module.exports = app;
