const todoModel = require("../models/todo"),
  jwt = require("jsonwebtoken");

function getId(req) {
  var token = req.headers.authorization.split("Bearer ")[1];

  let id;
  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
    id = decoded.id;
  });

  return id;
}

exports.getTodo = async (req, res) => {
  const response = await todoModel.find();
  let id = getId(req);
  let sendBack = [];
  response.map((element) => {
    if (element.userId === id) {
      sendBack.push(element);
    }
  });

  return res.status(200).json({ message: "Success", body: sendBack });
};

exports.createTodo = async (req, res) => {
  const { title } = req.body;

  let id = getId(req);
  if (!title)
    return res.status(400).json({ message: "Please input the title!" });

  try {
    await todoModel.create({
      title,
      done: false,
      userId: id,
    });

    const response = await todoModel.find({ title });

    return res.status(201).json({ message: "Success", body: response[0] });
  } catch (e) {
    return res.send(e);
  }
};

exports.putTodo = async (req, res) => {
  const { id: _id } = req.params;
  let id = getId(req);

  const response = await todoModel.findById(_id);

  if (response.userId !== id) {
    return res.status(400).json({ message: "This Todo List is not yours!" });
  }

  if (response) {
    await todoModel.updateOne({ _id }, { done: !response.done });
    const response2 = await todoModel.findById(_id);

    return res.status(200).json({ message: "Success", body: response2 });
  } else {
    return res.status(400).json({ message: "Failed, ID not found" });
  }
};

exports.getTodoId = async (req, res) => {
  const { id: _id } = req.params;
  const response = await todoModel.findById(_id);

  let id = getId(req);

  if (response.userId !== id) {
    return res.status(400).json({ message: "This Todo List is not yours!" });
  }

  if (response) {
    return res.status(200).json({ message: "Success", body: response });
  } else {
    return res.status(400).json({ message: "Failed, ID not found" });
  }
};

exports.deleteTodo = async (req, res) => {
  const { id: _id } = req.params;
  const response = await todoModel.findByIdAndDelete(_id);

  let id = getId(req);

  if (response.userId !== id) {
    return res.status(400).json({ message: "This Todo List is not yours!" });
  }
  
  if (response) {
    return res.status(201).json({ message: "Success", body: response });
  } else {
    return res.status(400).json({ message: "Failed, ID not found" });
  }
};
