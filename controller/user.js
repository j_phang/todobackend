const userModel = require("../models/user"),
  jwt = require("jsonwebtoken");

function getId(req) {
  var token = req.headers.authorization.split("Bearer ")[1];

  let id;
  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, decoded) => {
    id = decoded.id;
  });

  return id;
}

exports.createUser = async (req, res) => {
  const { username, password } = req.body;
  try {
    const response = await userModel.create({
      username,
      password,
      token: "",
    });
    res.status(200).json({
      status: "Success!",
      data: response,
    });
  } catch (err) {
    res.send("Username/Password Wrong!");
  }
};

exports.loginUser = async (req, res) => {
  const { username, password } = req.body;
  console.log({ username, password });
  let account = await userModel.findOne({ username });

  if (!username || !password) {
    return res
      .status(400)
      .json({ message: "Please Enter Your Email/Password" });
  }

  if (password === account.password) {
    const response = jwt.sign({ id: account.id }, process.env.JWT_SECRET_KEY);
    await userModel.updateOne({ token: response });

    return res.status(200).json({ message: "Hello There!", token: response });
  } else {
    return res.status(400).json({ message: "Username/Password is wrong" });
  }
};

exports.updateUser = async (req, res) => {
  const { username, password, newPassword } = req.body;
  let id = getId(req);
  
  let account = await userModel.findOne({ _id: id });

  if (!username || !password) {
    return res
      .status(400)
      .json({ message: "Please Enter Your Email/Password" });
  }

  if (password === account.password) {
    await userModel.updateOne({ _id: id }, { password: newPassword });
    const newAccount = await userModel.findOne({ _id: id });
    return res
      .status(200)
      .json({ message: "Hello There!", account: newAccount });
  } else {
    return res.status(400).json({ message: "Username/Password is wrong" });
  }
};
