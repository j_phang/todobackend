require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const userRouter = require("./router/user");
const todoRouter = require("./router/todo");
const cors = require('cors');
// database connection
const env = process.env.NODE_ENV || "development";

const configDB = {
  development: process.env.DB_DEV,
  production: process.env.DB_PROD,
};

const dbConnection = configDB[env];

mongoose.set("useCreateIndex", true);
mongoose.set("useFindAndModify", false);
mongoose.set("useNewUrlParser", true);
mongoose.set("useUnifiedTopology", true);
mongoose.connect(dbConnection);

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use("/user", userRouter);
app.use("/todo", todoRouter);

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening to port ${port}`));
